# PepalClassRoom



## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/hackaton-15/pepalclassroom.git
git branch -M main
git push -uf origin main
```

## Introduction

Le but est de proposer une plateforme web d'hébergement et d'éditions de cours en ligne.
Un utilisateur peut avoir plusieurs rôles : Etudiant, Enseignant, Admin.
Les cours sont éditables depuis la plateforme et classable selon le cours, chaque cours peut contenir plusieurs chapitres et sections.

Côté back-end, la base de données et l'API sont fournies.
Une CI/CD est mise en place pour lancer une batterie de test Kubernetes.
Une image Docker de notre environnement est mise en place également.

Trois types de tests seront lancés :
- Sur l'API externe
- Sur l'interface graphique avec Cypress
- Sur la qualtié du code avec Linter

Le job de la CI/CD permettra de lancer des merge request une fois les tests terminés.